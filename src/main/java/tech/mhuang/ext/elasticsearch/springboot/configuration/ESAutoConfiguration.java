package tech.mhuang.ext.elasticsearch.springboot.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.core.check.CheckAssert;
import tech.mhuang.ext.elasticsearch.admin.ESFramework;
import tech.mhuang.ext.elasticsearch.admin.external.IESExternal;
import tech.mhuang.ext.spring.start.SpringContextHolder;
import tech.mhuang.ext.springboot.context.SpringBootExtAutoConfiguration;

/**
 * es构建类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(ESFramework.class)
@EnableConfigurationProperties(ESProperties.class)
@ConditionalOnProperty(prefix = "mhuang.elasticsearch", name = "enable", havingValue = "true", matchIfMissing = true)
@AutoConfigureAfter({SpringBootExtAutoConfiguration.class})
@Slf4j
public class ESAutoConfiguration {

    private final ESProperties properties;

    public ESAutoConfiguration(ESProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean
    public IESExternal esExternal() {
        return new SpringESExternal();
    }

    /**
     * 确保SpringContextHolader加载到spring 容器
     *
     * @param esExternal
     * @param springContextHolder
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public ESFramework esFramework(IESExternal esExternal, SpringContextHolder springContextHolder) {
        CheckAssert.check(this.properties, "es properties invalid...");
        CheckAssert.check(springContextHolder, "SpringContextHolder不存在、请设置mhuang.holder.enable=true");
        ESFramework framework = new ESFramework(this.properties);
        framework.external(esExternal);
        framework.start();
        return framework;
    }
}
