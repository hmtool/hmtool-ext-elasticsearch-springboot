package tech.mhuang.ext.elasticsearch.springboot.configuration;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.ext.elasticsearch.admin.bean.ESInfo;

/**
 * elasticsearch属性配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = "mhuang.elasticsearch")
public class ESProperties extends ESInfo {

    /**
     * auto open elasticsearch enable.
     * default enable false
     */
    private boolean enable;
}
