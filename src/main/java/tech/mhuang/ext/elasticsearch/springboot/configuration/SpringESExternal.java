package tech.mhuang.ext.elasticsearch.springboot.configuration;

import tech.mhuang.ext.elasticsearch.admin.external.IESExternal;
import tech.mhuang.ext.elasticsearch.admin.factory.IESFactory;
import tech.mhuang.ext.elasticsearch.server.ESFactory;
import tech.mhuang.ext.spring.start.SpringContextHolder;

/**
 * spring es扩展
 *
 * @author mhuang
 * @since 1.0.0
 */
public class SpringESExternal implements IESExternal {

    @Override
    public IESFactory create(String key) {
        return SpringContextHolder.registerBean(key, ESFactory.class);
    }
}
